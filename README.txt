
#1)Nombres de los integrantes del grupo:

	Luis Veliz

	Gian Luca Bellone

	Mauro Rosales

	Federico Cortez

#2)Decisiones de diseño tomadas

	Se uso menu por consola para mostrar las acciones de cada metodo u objeto que se invoca cuando se ejecuta el codigo.

#3)Paquetes dentro de la carpeta src/main/java#

#Paquete Excepciones

Dentro del paquete hay 4 clases: 	

*Lanzan Exepciones de los metodos que pueden lanzar Exepciones*



#Paquete Elementos

Dentro del paquete hay 4 clases del elementos y la clase elemento

La clase Agua es un elemento 
La clase Aire es un elemento
La clase Fuego es un elemento
La clase Tierra es un elemento
Todas las clases heredan de elemento
La clase elemento Tiene los metodos 
	*Nombre: Muestra el nombre del elemento*
	*Debilidad: Un elemento es debil a otro*
	*FuerteA: Un elemento es fuerte a otro*
	
	
#Paquete Juego

#BatallaDeMonstruos#
		Batalla de Monstruos
	
*El Constructor recibe como parametro Dos jugadores*

		El metodo 	ElegirElemento
*Tiene 4 meteodos que son ElegirElemento , cada uno con un elemento diferente:*

Hace que los jugadores elijan un elemento.

		El metodo CrearMounstruos
		
*Crea un monstruo con dos elementos* //Los elementos tienen que estar definidos

		El metodos Ataque

*Se elige un ataque del 1 al 4*  

		Jugador hayGanador

*Indica quien fue el jugador que gano*

		hayUnGanador
*pregunta si hay ganado o no*

		ataqueDelJugador
		
*Muestra los ataques del jugador*	
	
		mostrarMonstruoDelJugador
		
*Muestra el monstruo del jugador*//Primero se tiene que crear un monstruo		
		
		
		
		
#Jugador#
	
	jugador
	
*El constructor Jugador recibe el nombre del jugador*

	CrearMonstruo	

*El metodo recibe como parametro el elemento 1 y el elemento 2*

	obtenerVida
	
*Devuelve la vida del monstruo*			
		

	elegirAtaque		
		
*El jugador elige un ataque del 1 al 4 o lanza una excepcion*

		esGanador

*El ganador es true*

	obtenerNombre

*Devuelve el nombre del jugador*

	mostrarMonstruo

*Devuelve mimonstruo*


#Monstruo#
	
	Monstruo
*El constructor recibe dos elementos*

	meAtacan

*El recibe al jugador que esta atacando al monstruo* 

	mostrarVida

*Muestra la vida del monstruo*

#4) Conclusiones 
El trabajo al comienzo presento muchos problemas , con los test , el UML y usando lo aprendido en clase , se comenzaron a resolver . Se logro mejorar el estilo de programacion y se tomo en cuenta la eficiencia. 

Nos ayudo a reforzar lo aprendi en clase. Se implemento herencia y excepciones que sirvieron para practicar y entender mas el tema. El problema de crear una batalla de monstruo y que dos jugadores interactuen entre si , creando un monstruo con elementos, nos ayudo a plantear el problema en un UML. Con la ayuda de git , pudimos trabajar mejor y estar sincronizados.

Para terminar, el Trabajo Practico nos ense�o a trabajar en conjunto y descubrir nuevas formas de encarar un problema .     






package Elementos;

public class Agua extends Elemento {

	private String elementoQueMeHaceDano = "Aire";
	 private String elementoQueHagoDano = "Fuego";
	private String nombre;

	public Agua() {
		nombre = "Agua";

	}

	public String nombre() {
		return nombre;
	}

	public String debilidad() {

		return elementoQueMeHaceDano;

	}

	public String fuerteA() {

		return elementoQueHagoDano;
	}

}

package Elementos;

public class Aire extends Elemento {

	private String elementoQueMeHaceDano = "Tierra";
	private String elementoQueHagoDano = "Agua";
	private String nombre;

	public Aire() {
		nombre = "Aire";

	}

	public String nombre() {
		return nombre;
	}

	public String debilidad() {


		return elementoQueMeHaceDano;
	}

	public String fuerteA() {

		return elementoQueHagoDano;

	}
}

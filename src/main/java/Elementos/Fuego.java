package Elementos;

public class Fuego extends Elemento {

	private String elementoQueMeHaceDano = "Agua";
	private String elementoQueHagoDano = "Tierra";
	private String nombre;

	public Fuego() {
		nombre = "Fuego";

	}

	public String nombre() {
		return nombre;
	}

	public String debilidad() {

		return elementoQueMeHaceDano;

	}

	public String fuerteA() {

		return elementoQueHagoDano;

	}
}

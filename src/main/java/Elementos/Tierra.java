package Elementos;

public class Tierra extends Elemento {

	
	private String elementoQueMeHaceDano = "Fuego";
	private String elementoQueHagoDano = "Aire";
	private String nombre;


	public Tierra() {
		nombre = "Tierra";

	}

	public String nombre() {
		return nombre;
	}


	public String debilidad() {
		
		return elementoQueMeHaceDano;
	}


	public String fuerteA() {
		
		return elementoQueHagoDano;
		
	}

}

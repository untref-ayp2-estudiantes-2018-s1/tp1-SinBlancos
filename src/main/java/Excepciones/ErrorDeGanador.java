package Excepciones;

public class ErrorDeGanador extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorDeGanador() {
		super("Ya hay un ganador");
	}
}

package Juego;

import Elementos.Agua;
import Elementos.Aire;
import Elementos.Elemento;
import Elementos.Fuego;
import Elementos.Tierra;
import Excepciones.ErrorCrearElementos;
import Excepciones.ErrorCrearMonstruo;
import Excepciones.ErrorDeAtaque;
import Excepciones.ErrorDeGanador;

public class BatallaDeMonstruos {

	public Jugador j1;
	public Jugador j2;
	public Elemento elemento1;
	public Elemento elemento2;
	public Jugador ganador;
	public Monstruo monstruoJ1;
	public Monstruo monstruoJ2;
	public boolean turnoDelJugador1;
	public int turnoDeCreacion;
	public int numeroDeAtaque;
	public boolean elemento;
	String jugador1;
	String jugador2;
	boolean elGanador = false;

	public BatallaDeMonstruos(String jugador1, String jugador2) {
		this.jugador2 = jugador2;
		this.jugador1 = jugador1;
		j1 = new Jugador(jugador1);
		j2 = new Jugador(jugador2);
		j1.JugadorEnemigo = j2;
		j2.JugadorEnemigo = j1;
		turnoDelJugador1 = true;

		System.out.println("Se creo el jugador 1 con el nombre " + jugador1);
		System.out.println("Se creo el jugador 2 con el nombre " + jugador2);

	}

	public void elegirElementoAgua() {

		Elemento agua = new Agua();
		if (elemento1 == null) {
			elemento1 = new Agua();
		}
		if (elemento1 != null && elemento1 != agua) {
			elemento2 = new Agua();
		}
		elemento = true;

	}

	public void elegirElementoFuego() {

		Elemento fuego = new Fuego();
		if (elemento1 == null) {
			elemento1 = new Fuego();
		}
		if (elemento1 != null && elemento1 != fuego) {
			elemento2 = new Fuego();

		}
		elemento = true;
	}

	public void elegirElementoAire() {
		Elemento aire = new Aire();
		if (elemento1 == null) {
			elemento1 = new Aire();
		}
		if (elemento1 != null && elemento1 != aire) {
			elemento2 = new Aire();

		}
		elemento = true;
	}

	public void elegirElementoTierra() {

		Elemento tierra = new Tierra();
		if (elemento1 == null) {
			elemento1 = new Tierra();
			elemento = true;
		}
		if (elemento1 != null && elemento1 != tierra) {
			elemento2 = new Tierra();

		}
		elemento = true;
	}

	public void noElegirElemento() {

		if (elemento1 != null) {
			elemento2 = null;

		}
		elemento = true;

	}

	public void crearMounstruos() throws Exception {
		turnoDeCreacion = 0;

		try {

			if (turnoDelJugador1 == true && turnoDeCreacion == 0 && elemento == true) {

				j1.crearMonstruo(elemento1, elemento2);
				monstruoJ1 = j1.mostrarMonstruo();
				mostrarMonstruoDelJugador();
				elemento1 = null;
				elemento2 = null;

				turnoDelJugador1 = false;

				turnoDeCreacion++;
			} else if (elemento == false) {
				throw new ErrorCrearElementos();
			}

			if (turnoDelJugador1 == false && turnoDeCreacion == 0) {
				j2.crearMonstruo(elemento1, elemento2);
				monstruoJ2 = j2.mostrarMonstruo();
				mostrarMonstruoDelJugador();
				elemento1 = null;
				elemento2 = null;
				turnoDelJugador1 = true;

				turnoDeCreacion++;
			}
		}

		catch (ErrorCrearElementos b) {
			System.out.println(b);
		}

	}

	public void ataque(int nroDelAtaque) throws Exception {
		int turno = 0;
		this.numeroDeAtaque = nroDelAtaque;

		try {

			if (nroDelAtaque > 0 && nroDelAtaque <= 4) {

				try {
					if (turnoDelJugador1 == true && hayGanador() == null && turno == 0 && monstruoJ2 != null) {
						j1.elegirAtaque(nroDelAtaque);
						monstruoJ2.meAtacan(j1);
						ataqueDelJugador();
						turnoDelJugador1 = false;

						turno++;

					}

					if (turnoDelJugador1 == false && hayGanador() == null && turno == 0 && monstruoJ1 != null) {
						j2.elegirAtaque(nroDelAtaque);
						monstruoJ1.meAtacan(j2);
						ataqueDelJugador();
						turnoDelJugador1 = true;

						turno++;

					}

					if (hayGanador() != null) {
						System.out.println("Gano el jugador: " + hayGanador().getNombre());
					}
					if (monstruoJ1 == null || monstruoJ2 == null) {
						throw new ErrorCrearMonstruo();
					}

				} catch (ErrorDeGanador s) {
					System.out.println(s);

				}

				catch (ErrorCrearMonstruo e) {
					System.out.println(e);
				}
			} else {
				j1.errorDeAtaque();
			}

		} catch (ErrorDeAtaque e) {
			System.out.println(e);

		}

	}

	public Jugador hayGanador() {

		if (j1.obtenerVida() <= 0) {
			j2.esGanador();
			ganador = j2;
		} else if (j2.obtenerVida() <= 0) {
			j1.esGanador();
			ganador = j1;
		}

		return ganador;
	}

	public boolean hayUnGanador() {

		if (j1.obtenerVida() <= 0 || j2.obtenerVida() <= 0) {
			elGanador = true;
		}
		return elGanador;

	}

	public void ataqueDelJugador() {

		if (turnoDelJugador1 == true && j1.obtenerVida() != 0) {

			if (numeroDeAtaque == 1 || numeroDeAtaque == 2) {
				System.out.println(jugador1 + " elige el ataque " + numeroDeAtaque + " del elemento "
						+ j1.getElemento1().nombre());
			} else {
				System.out.println(jugador1 + " elige el ataque " + numeroDeAtaque + " del elemento "
						+ j1.getElemento2().nombre());
				
			}
			if (j2.obtenerVida() != 0) {
				System.out.println("El monstruo de " + jugador2 + " recibe " + j2.getMiMonstruo().ataque

						+ " de dano, al monstruo le queda " + j2.obtenerVida() + " de vida");
				
			}


		} else if (j1.obtenerVida() != 0) {

			if (numeroDeAtaque == 1 || numeroDeAtaque == 2) {
				System.out.println(jugador2 + " elige el ataque " + numeroDeAtaque + " del elemento "
						+ j2.getElemento1().nombre());
			} else {
				System.out.println(jugador2 + " elige el ataque " + numeroDeAtaque + " del elemento "
						+ j2.getElemento2().nombre());

			}
			System.out.println("El monstruo de " + jugador1 + " recibe " + j1.getMiMonstruo().ataque
					+ " de dano, al monstruo le queda " + j1.obtenerVida() + " de vida");

			

		}

	}

	public void mostrarMonstruoDelJugador() {
		if (turnoDelJugador1 == true) {
			if (j1.getElemento2() == null) {
				System.out.println(
						jugador1 + " creo un monstruo y elegio el elemento de tipo " + j1.getElemento1().nombre());
			}
			if(j1.getElemento2() != null) {	
			System.out.println(jugador1 + " creo un monstruo y elegio el elemento de tipo " + j1.getElemento1().nombre()
					+ " y " + j1.getElemento2().nombre());
			}
		} else {
			if (j2.getElemento2() == null) {
				System.out.println(
						jugador2 + " creo un monstruo y elegio el elemento de tipo " + j2.getElemento1().nombre());
			}

			if(j2.getElemento2() != null) {
				System.out.println(jugador2 + " creo un monstruo y elegio el elemento de tipo " + j2.getElemento1().nombre()
						+ " y " + j2.getElemento2().nombre());
			}

		}

	}

}
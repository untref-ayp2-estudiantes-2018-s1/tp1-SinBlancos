package Juego;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BattleMonster {

	public BattleMonster() throws Exception {

		System.out.println("Introduzca nombre de jugador 1");
		BufferedReader j1 = new BufferedReader(new InputStreamReader(System.in));
		String jugador1 = j1.readLine();

		System.out.println("Introduzca nombre de jugador 2");
		BufferedReader j2 = new BufferedReader(new InputStreamReader(System.in));
		String jugador2 = j2.readLine();

		BatallaDeMonstruos batalla = new BatallaDeMonstruos(jugador1, jugador2);

		System.out.println("Elija el primer elemento del monstruo de " + jugador1 + "(1-4)");
		System.out.println("1. Agua");
		System.out.println("2. Fuego");
		System.out.println("3. Tierra");
		System.out.println("4. Aire");

		BufferedReader e1j1 = new BufferedReader(new InputStreamReader(System.in));
		String e1 = e1j1.readLine();

		if (e1.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (e1.contains("2")) {
			batalla.elegirElementoFuego();
		}
		if (e1.contains("3")) {
			batalla.elegirElementoTierra();
		}
		if (e1.contains("4")) {
			batalla.elegirElementoAire();
		}

		System.out.println("Elija el Segundo elemento del monstruo de " + jugador1 + "(1-4)");
		if (e1.contains("1")) {
		System.out.println("1. Fuego");
		System.out.println("2. Tierra");
		System.out.println("3. Aire");
		System.out.println("4. Ninguno");

		BufferedReader e2j1 = new BufferedReader(new InputStreamReader(System.in));
		String e2 = e2j1.readLine();

		if (e2.contains("1")) {
			batalla.elegirElementoFuego();
		}
		if (e2.contains("2")) {
			batalla.elegirElementoTierra();
		}
		if (e2.contains("3")) {
			batalla.elegirElementoAire();
		}
		if (e2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		if (e1.contains("2")) {
		System.out.println("1. Agua");
		System.out.println("2. Tierra");
		System.out.println("3. Aire");
		System.out.println("4. Ninguno");

		BufferedReader e2j1 = new BufferedReader(new InputStreamReader(System.in));
		String e2 = e2j1.readLine();

		if (e2.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (e2.contains("2")) {
			batalla.elegirElementoTierra();
		}
		if (e2.contains("3")) {
			batalla.elegirElementoAire();
		}
		if (e2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		if (e1.contains("3")) {
		System.out.println("1. Agua");
		System.out.println("2. Fuego");
		System.out.println("3. Aire");
		System.out.println("4. Ninguno");

		BufferedReader e2j1 = new BufferedReader(new InputStreamReader(System.in));
		String e2 = e2j1.readLine();

		if (e2.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (e2.contains("2")) {
			batalla.elegirElementoFuego();
		}
		if (e2.contains("3")) {
			batalla.elegirElementoAire();
		}
		if (e2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		if (e1.contains("4")) {
		System.out.println("1. Agua");
		System.out.println("2. Fuego");
		System.out.println("3. Tierra");
		System.out.println("4. Ninguno");

		BufferedReader e2j1 = new BufferedReader(new InputStreamReader(System.in));
		String e2 = e2j1.readLine();

		if (e2.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (e2.contains("2")) {
			batalla.elegirElementoFuego();
		}
		if (e2.contains("3")) {
			batalla.elegirElementoTierra();
		}
		if (e2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		batalla.crearMounstruos();

		System.out.println("Elija el primer elemento del monstruo de " + jugador2 + "(1-4)");
		System.out.println("1. Agua");
		System.out.println("2. Fuego");
		System.out.println("3. Tierra");
		System.out.println("4. Aire");

		BufferedReader e1j2 = new BufferedReader(new InputStreamReader(System.in));
		String el1 = e1j2.readLine();

		if (el1.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (el1.contains("2")) {
			batalla.elegirElementoFuego();
		}
		if (el1.contains("3")) {
			batalla.elegirElementoTierra();
		}
		if (el1.contains("4")) {
			batalla.elegirElementoAire();
		}

		System.out.println("Elija el Segundo elemento del monstruo de " + jugador2 + "(1-4)");
		if(el1.contains("1")) {
		System.out.println("1. Fuego");
		System.out.println("2. Tierra");
		System.out.println("3. Aire");
		System.out.println("4. Ninguno");

		BufferedReader e2j2 = new BufferedReader(new InputStreamReader(System.in));
		String el2 = e2j2.readLine();

		if (el2.contains("1")) {
			batalla.elegirElementoFuego();
		}
		if (el2.contains("2")) {
			batalla.elegirElementoTierra();
		}
		if (el2.contains("3")) {
			batalla.elegirElementoAire();
		}
		if (el2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		if(el1.contains("2")) {
		System.out.println("1. Agua");
		System.out.println("2. Tierra");
		System.out.println("3. Aire");
		System.out.println("4. Ninguno");

		BufferedReader e2j2 = new BufferedReader(new InputStreamReader(System.in));
		String el2 = e2j2.readLine();

		if (el2.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (el2.contains("2")) {
			batalla.elegirElementoTierra();
		}
		if (el2.contains("3")) {
			batalla.elegirElementoAire();
		}
		if (el2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		if(el1.contains("3")) {
		System.out.println("1. Agua");
		System.out.println("2. Fuego");
		System.out.println("3. Aire");
		System.out.println("4. Ninguno");

		BufferedReader e2j2 = new BufferedReader(new InputStreamReader(System.in));
		String el2 = e2j2.readLine();

		if (el2.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (el2.contains("2")) {
			batalla.elegirElementoFuego();
		}
		if (el2.contains("3")) {
			batalla.elegirElementoAire();
		}
		if (el2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		if(el1.contains("4")) {
		System.out.println("1. Agua");
		System.out.println("2. Fuego");
		System.out.println("3. Tierra");
		System.out.println("4. Ninguno");

		BufferedReader e2j2 = new BufferedReader(new InputStreamReader(System.in));
		String el2 = e2j2.readLine();

		if (el2.contains("1")) {
			batalla.elegirElementoAgua();
		}
		if (el2.contains("2")) {
			batalla.elegirElementoFuego();
		}
		if (el2.contains("3")) {
			batalla.elegirElementoTierra();
		}
		if (el2.contains("4")) {
			batalla.noElegirElemento();
		}
		}
		batalla.crearMounstruos();
		
		while(batalla.hayGanador() == null) {
			System.out.println("Turno de " + jugador1 );
			System.out.println("Elija un ataque");
			System.out.println("1. Ataque normal de tipo " + batalla.j1.getElemento1().nombre() );
			System.out.println("2. Ataque especial de tipo " + batalla.j1.getElemento1().nombre() );
			if(batalla.j1.getElemento2() != null) {
			System.out.println("3. Ataque normal de tipo " + batalla.j1.getElemento2().nombre() );
			System.out.println("4. Ataque especial de tipo " + batalla.j1.getElemento2().nombre() );
			}

			Scanner scanj1 = new Scanner (System.in);
			int nroj1 =0;
			nroj1 = scanj1.nextInt();
			batalla.ataque(nroj1);
			
			
			
			System.out.println("Turno de " + jugador2);
			System.out.println("Elija un ataque");
			System.out.println("1. Ataque normal de tipo " + batalla.j2.getElemento1().nombre() );
			System.out.println("2. Ataque especial de tipo " + batalla.j2.getElemento1().nombre() );
			if(batalla.j2.getElemento2() != null) {
			System.out.println("3. Ataque normal de tipo " + batalla.j2.getElemento2().nombre() );
			System.out.println("4. Ataque especial de tipo " + batalla.j2.getElemento2().nombre() );
			}
			Scanner scanj2 = new Scanner (System.in);
			int nroj2 =0;
			nroj2 = scanj2.nextInt();

			batalla.ataque(nroj2);
		
		}
	}

}

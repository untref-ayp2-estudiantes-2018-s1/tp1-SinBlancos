package Juego;

import Elementos.Elemento;
import Excepciones.ErrorDeAtaque;
import Excepciones.ErrorMounstruoCreado;

public class Jugador {
	private String nombre;

	private String tipoDeAtaque;
	private Elemento tipoDeElemento;
	private Elemento elemento1;
	private Elemento elemento2;
	private Monstruo miMonstruo;
	boolean esGanador = false;
	boolean ataco;
	String nombreDelMonstruo;
	Jugador JugadorEnemigo;

	int maxAtkEspeciales = 0;

	public Jugador(String nombre) {

		this.nombre = nombre;

	}

	public void crearMonstruo(Elemento e1, Elemento e2) throws Exception { // Crea un mounstruo, en el caso de
																			// que ya haya creado uno tira
																			// error.

		try {

			if (miMonstruo != null) {
				mounstruoCreado();

			} else {
				miMonstruo = new Monstruo(e1, e2);
				elemento1 = miMonstruo.getElemento1();
				elemento2 = miMonstruo.getElemento2();
			}
		} catch (ErrorMounstruoCreado e) {
			System.out.println(e);
		}

	}

	public double obtenerVida() {
		double vida = miMonstruo.mostrarVida();
		return vida;

	}

	public void elegirAtaque(int nroDelAtaque) throws Exception {

		try {
			if (nroDelAtaque < 0 || nroDelAtaque > 4) {

				errorDeAtaque();
			}

			else {

				if (nroDelAtaque == 1) {

					tipoDeAtaque = "Normal";
					tipoDeElemento = elemento1;

				}
				if (nroDelAtaque == 2 && maxAtkEspeciales < 4) {

					tipoDeAtaque = "Especial";
					tipoDeElemento = elemento1;
					maxAtkEspeciales++;

				}

				if (elemento2 != null && nroDelAtaque == 3) {

					tipoDeAtaque = "Normal";
					tipoDeElemento = elemento2;
				}
				if (elemento2 != null && nroDelAtaque == 4 && maxAtkEspeciales < 4) {

					tipoDeAtaque = "Especial";
					tipoDeElemento = elemento2;
					maxAtkEspeciales++;
				}

				ataco = true;
			}
		} catch (ErrorDeAtaque e) {
			System.out.println(e);
		}

	}

	public boolean esGanador() {

		return esGanador = true;
	}

	public String obtenerNombre() {
		return nombre;
	}

	Monstruo mostrarMonstruo() {

		return miMonstruo;
	}

	private void mounstruoCreado() throws Exception {
		throw new ErrorMounstruoCreado();

	}

	public void errorDeAtaque() throws Exception {
		throw new ErrorDeAtaque();

	}


	public Monstruo getMiMonstruo() {
		return miMonstruo;
}

	public void setMiMonstruo(Monstruo miMonstruo) {
		this.miMonstruo = miMonstruo;
}

	public Jugador getJugadorEnemigo() {
		return JugadorEnemigo;
}

	public void setJugadorEnemigo(Jugador jugadorEnemigo) {
		JugadorEnemigo = jugadorEnemigo;
}

	public String getNombre() {
		return nombre;
}

	public String getTipoDeAtaque() {
		return tipoDeAtaque;
}

	public Elemento getTipoDeElemento() {
		return tipoDeElemento;
}

	public Elemento getElemento1() {
		return elemento1;
}

	public Elemento getElemento2() {
		return elemento2;
}

	public boolean isEsGanador() {
		return esGanador;
}

	public boolean isAtaco() {
		return ataco;
}

	public String getNombreDelMonstruo() {
		return nombreDelMonstruo;
}

	public int getMaxAtkEspeciales() {
		return maxAtkEspeciales;
}
}
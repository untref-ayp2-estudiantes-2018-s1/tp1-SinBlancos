package Juego;

import Elementos.Elemento;

public class Monstruo {

	private Elemento elemento1;
	private Elemento elemento2;
	private double vida = 100;
	private Jugador quienMeAtaca;
	int nroDeAtaquesDisponibles;
	int ataque = 0;
	

	public Monstruo(Elemento e1, Elemento e2  ) {

		elemento1 = e1;
		elemento2 = e2;
		

		
	}


	public void meAtacan(Jugador quienMeAtaca) {
		this.quienMeAtaca = quienMeAtaca;
		if (quienMeAtaca.ataco == true && elemento2 != null) {
			String elementoAtacante = quienMeAtaca.getTipoDeElemento().nombre() ;
			String debilidad = elemento1.debilidad();
			String debilidad2 = elemento2.debilidad();
			String fortaleza1 = elemento1.fuerteA();
			String fortaleza2 = elemento2.fuerteA();
			
			if (elementoAtacante == debilidad
					|| elementoAtacante == debilidad2) {            // Verifico que el elemento es el que me causa mas dano
				if (quienMeAtaca.getTipoDeAtaque() == "Normal") {
					vida = vida - 12;   
					ataque = 12;//Se resta el ataque normal mas el %20
				}
				if (quienMeAtaca.getTipoDeAtaque() == "Especial") {
					vida = vida - 18;  
					ataque= 18;//Se resta el ataque especial mas el %20
				}
			} 
			else if (elementoAtacante == fortaleza1
					|| elementoAtacante == fortaleza2) {           // Verifico que el elemento es el que me causa menos dano

				if (quienMeAtaca.getTipoDeAtaque() == "Normal") {
					vida = vida - 8; 
					ataque= 8;//Se resta el ataque normal menos el %20
				}
				if (quienMeAtaca.getTipoDeAtaque() == "Especial") {
					vida =  vida - 13; 
					ataque= 13;//Se Resta el ataque especial menos el %20
				}
			}  
			else if (quienMeAtaca.getTipoDeAtaque() == "Normal") {

				vida = vida -10;
				ataque= 10;

			} else if (quienMeAtaca.getTipoDeAtaque()== "Especial") {

				vida = vida- 15;
				ataque= 15;
			}

		}
		quienMeAtaca.ataco = false;

	}
	
	

	public double mostrarVida() {
		if(vida < 0) {
			vida = 0;
		}
		return vida;

	}
	public Elemento getElemento1() {
		return elemento1;
	}


	public Elemento getElemento2() {
		return elemento2;
	}


	public double getVida() {
		return vida;
	}


	public Jugador getQuienMeAtaca() {
		return quienMeAtaca;
	}


	public int getNroDeAtaquesDisponibles() {
		return nroDeAtaquesDisponibles;
	}


	public int getAtaque() {
		return ataque;
	}

}

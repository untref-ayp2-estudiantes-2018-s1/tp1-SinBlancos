package Test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Juego.BatallaDeMonstruos;

public class BatallaDeMonstruosTest {

	@Test(expected = Exception.class)
	public void noSeCreanElementos() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");
		b.elegirElementoAgua();

		b.crearMounstruos(); 

		b.crearMounstruos();

	}

	@Test
	public void GanoElJugadoUnoConAtaqueEspeciales() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		b.elegirElementoAire();
		b.elegirElementoFuego();

		b.crearMounstruos();

		b.elegirElementoAgua();
		b.elegirElementoTierra();

		b.crearMounstruos();

		b.ataque(2);// j1 ataco a j2
		b.ataque(2);// j2 ataco a j1

		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);
		b.ataque(2);

		assertEquals(b.j1.obtenerVida(), 10.0, 0.1);

		assertEquals(b.j2.obtenerVida(), 0.0, 0.1);
		assertEquals(b.hayGanador().obtenerNombre(), "Gian");

	}

	@Test
	public void GanaElJugadorDos() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		b.elegirElementoAire();
		b.elegirElementoFuego();

		b.crearMounstruos();

		b.elegirElementoAgua();
		b.elegirElementoTierra();

		b.crearMounstruos();

		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		b.ataque(2);
		b.ataque(1);
		assertEquals(b.j1.obtenerVida(), 0.0, 0.1);

		assertEquals(b.j2.obtenerVida(), 4.0, 0.1);

		assertEquals(b.hayGanador().obtenerNombre(), "Luis");

	}

	@Test(expected = Exception.class)
	public void NoSeCreaElMonstruoDelJugador2() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		b.elegirElementoAire();
		b.elegirElementoFuego();

		b.crearMounstruos();
		b.ataque(1);

	}

	@Test
	public void seCreaLosJugadores1Y2() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");

		assertEquals(b.j1.obtenerNombre(), "Gian");

		assertEquals(b.j2.obtenerNombre(), "Luis");

	}

	@Test
	public void seCreaLosJugadores1Y2YNoHayGanador() throws Exception {

		BatallaDeMonstruos b = new BatallaDeMonstruos("Gian", "Luis");
		// jugador 1
		b.elegirElementoAgua();
		b.elegirElementoFuego();
		b.crearMounstruos();
		// jugador 2
		b.elegirElementoAire();
		b.elegirElementoFuego();
		b.crearMounstruos();

		b.ataque(2);

		b.ataque(3);

		b.ataque(5);

		b.ataque(6);

		b.ataque(2);

		b.ataque(7);

		assertEquals(b.j1.obtenerNombre(), "Gian");

		assertEquals(b.j2.obtenerNombre(), "Luis");

		assertEquals(b.hayUnGanador(), false);
	}

}

package Test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Elementos.Agua;
import Elementos.Aire;
import Elementos.Elemento;
import Elementos.Fuego;
import Elementos.Tierra;
import Juego.Jugador;

public class JugadorTest {

	@Test
	public void SeCreaUnJugadorConMonstruos() throws Exception {

		Jugador j1 = new Jugador("Gian");

		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		j1.crearMonstruo(fuego, tierra);

		assertEquals(j1.getMiMonstruo().getElemento1(), fuego);
		assertEquals(j1.getMiMonstruo().getElemento2(), tierra);

	}

	@Test
	public void SeCreanDosJugadorConMonstruos() throws Exception {

		Jugador j1 = new Jugador("Gian");

		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		j1.crearMonstruo(fuego, tierra);
		Jugador j2 = new Jugador("Luis");

		Elemento aire = new Aire();
		Elemento agua = new Agua();
		j2.crearMonstruo(aire, agua);

		assertEquals(j1.getMiMonstruo().getElemento1(), fuego);
		assertEquals(j1.getMiMonstruo().getElemento2(), tierra);

		assertEquals(j2.getMiMonstruo().getElemento1(), aire);
		assertEquals(j2.getMiMonstruo().getElemento2(), agua);

	}

	@Test
	public void SeCreanDosMounstrosDeIgualElementos() throws Exception {

		Jugador j1 = new Jugador("Gian");

		Elemento fuego = new Fuego();

		j1.crearMonstruo(fuego, fuego);
		Jugador j2 = new Jugador("Luis");

		Elemento agua = new Agua();
		j2.crearMonstruo(agua, agua);

		assertEquals(j1.getMiMonstruo().getElemento1(), fuego);
		assertEquals(j1.getMiMonstruo().getElemento2(), fuego);

		assertEquals(j2.getMiMonstruo().getElemento1(), agua);
		assertEquals(j2.getMiMonstruo().getElemento2(), agua);

	}

	@Test
	public void SeCreanDosJugadoresConElMismoElementoYMismoNombre() throws Exception {

		Jugador j1 = new Jugador("Luis");

		Elemento fuego = new Fuego();

		j1.crearMonstruo(fuego, fuego);
		Jugador j2 = new Jugador("Luis");

		Elemento agua = new Agua();
		j2.crearMonstruo(agua, agua);

		assertEquals(j1.getMiMonstruo().getElemento1(), fuego);
		assertEquals(j1.getMiMonstruo().getElemento2(), fuego);

		assertEquals(j2.getMiMonstruo().getElemento1(), agua);
		assertEquals(j2.getMiMonstruo().getElemento2(), agua);

		assertEquals(j1.obtenerNombre(), "Luis");

		assertEquals(j2.obtenerNombre(), "Luis");

	}

	@Test
	public void SeCreanDosJugadoresConElMismoElementoYMismoNombreYLaVidaDeCadaUno() throws Exception {

		Jugador j1 = new Jugador("Luis");

		Elemento fuego = new Fuego();

		j1.crearMonstruo(fuego, fuego);
		Jugador j2 = new Jugador("Luis");

		Elemento agua = new Agua();
		j2.crearMonstruo(agua, agua);

		assertEquals(j1.getMiMonstruo().getElemento1(), fuego);
		assertEquals(j1.getMiMonstruo().getElemento2(), fuego);

		assertEquals(j2.getMiMonstruo().getElemento1(), agua);
		assertEquals(j2.getMiMonstruo().getElemento2(), agua);

		assertEquals(j1.obtenerNombre(), "Luis");

		assertEquals(j2.obtenerNombre(), "Luis");

		assertEquals(j1.obtenerVida(), 100, 0.0);
		assertEquals(j2.obtenerVida(), 100, 0.0);

	}

	@Test
	public void TresJugadores() throws Exception {

		Jugador j1 = new Jugador("Luis");

		Elemento fuego = new Fuego();
		Elemento aire = new Aire();

		j1.crearMonstruo(fuego, aire);
		Jugador j2 = new Jugador("Gian");

		Elemento tierra = new Tierra();
		Elemento agua = new Agua();
		j2.crearMonstruo(agua, tierra);
		
		Jugador j3 = new Jugador("Mauro");

		j3.crearMonstruo(agua, fuego);
		
		assertEquals(j1.getMiMonstruo().getElemento1(), fuego);
		assertEquals(j1.getMiMonstruo().getElemento2(), aire);

		assertEquals(j2.getMiMonstruo().getElemento1(), agua);
		assertEquals(j2.getMiMonstruo().getElemento2(), tierra);

		assertEquals(j1.obtenerNombre(), "Luis");

		assertEquals(j2.obtenerNombre(), "Gian");

		assertEquals(j1.obtenerVida(), 100, 0.0);
		assertEquals(j2.obtenerVida(), 100, 0.0);
		
			
		
	}

}

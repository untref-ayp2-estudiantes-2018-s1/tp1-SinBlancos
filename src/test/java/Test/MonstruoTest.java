package Test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Elementos.Agua;
import Elementos.Aire;
import Elementos.Elemento;
import Elementos.Fuego;
import Elementos.Tierra;
import Juego.Monstruo;

public class MonstruoTest {

	@Test
	public void SeCreaUnMonstruoConElementos() {
		Elemento agua = new Agua();
		Elemento tierra = new Tierra();

		Monstruo m1 = new Monstruo(agua, tierra);

		assertEquals(m1.getElemento1(), agua);
		assertEquals(m1.getElemento2(), tierra);

	}

	@Test
	public void SeCreaUnMonstruoConElementosIguales() {
		Elemento agua = new Agua();
		Elemento agua2 = new Agua();

		Monstruo m1 = new Monstruo(agua, agua2);

		assertEquals(m1.getElemento1(), agua);
		assertEquals(m1.getElemento2(), agua2);
	}

	@Test
	public void SeCreanDosMonstruoConElementos() {
		Elemento agua = new Agua();
		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		Elemento aire = new Aire();

		Monstruo m2 = new Monstruo(fuego, tierra);
		Monstruo m1 = new Monstruo(agua, aire);

		assertEquals(m1.getElemento1(), agua);

		assertEquals(m1.getElemento2(), aire);

		assertEquals(m2.getElemento1(), fuego);

		assertEquals(m2.getElemento2(), tierra);

	}

	@Test(expected = Error.class)

	public void SeCreanDosMonstruosConElementosInversos() throws Exception {
		

		Elemento agua = new Agua();
		Elemento fuego = new Fuego();
		Elemento tierra = new Tierra();
		Elemento aire = new Aire();

		Monstruo m1 = new Monstruo(agua, aire);
		Monstruo m2 = new Monstruo(fuego, tierra);

		
		assertEquals(m1.getElemento1(), tierra);

		assertEquals(m1.getElemento2(), fuego);

		assertEquals(m2.getElemento1(), agua);

		assertEquals(m2.getElemento2(), aire);
	}

}	
	
	